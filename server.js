var PORT = process.env.PORT || 8080;

var _ = require('underscore');
var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io').listen(server);

process.on('uncaughtException', function (err) {
  console.error(err.stack);
  console.log("Node Exiting...");
  process.exit(1);
});

var shutdownServer = function() {
  process.exit();
};

process.on('SIGINT', function () {
  console.log("SIGINT - Node Exiting...");
  shutdownServer();
});

process.on('SIGTERM', function () {
  console.log("SIGTERM - Node Exiting...");
  shutdownServer();
});

app.get('/', function(req, res) {
  console.log('GET /');
  res.sendfile('index.html');
});

io.enable('browser client minification');  // send minified client
io.enable('browser client etag');          // apply etag caching logic based on version number
io.enable('browser client gzip');          // gzip the file
io.set('log level', 1);                    // reduce logging

io.sockets.on('connection', function(client) {
  console.log(client.id + ' connected');
  
  
  client.on('ping', function(fn) {
    console.log(client.id + ' ping');
    fn('pong');
  });
  
  client.on('disconnect', function(data) {
    console.log(client.id + ' has disconnected');
  });
});

server.listen(PORT);
console.log('Listening on port ' + PORT);
